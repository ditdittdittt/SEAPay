run: build-ui
	bash -c "sbt run"

build: build-ui
	bash -c "sbt dist"

build-ui:
	bash -c "cd ./ui && npm run-script build"