![SEA Pay Logo](logo.png)


# SEA Pay

SEA Pay is an payment gateway application and marketplace built on top of Java [Play framework](https://www.playframework.com/) and [React Bootstrap](https://react-bootstrap.github.io/)

## Prerequisites

- [Node.js](https://nodejs.org/en/)
- [Java Development Kit](https://openjdk.java.net/projects/jdk/11/) (version 11)
- [sbt](https://www.scala-sbt.org/)

## Getting Started

You can run the application using

```
make run
```

## Building the Application

You can build the application using

```
make build
```

## Maintaineers

- [hsjsjsj009](https://github.com/hsjsjsj009)
- [andraantariksa](https://github.com/andraantariksa)
- [ditdittdittt](https://github.com/ditdittdittt)
- [achmadkhodzim](https://github.com/achmadkhodzim)