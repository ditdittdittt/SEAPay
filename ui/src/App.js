import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {connect} from 'react-redux';

// Component
import NavBar from './component/navbar';
import {
  PrivateRoute,
  PublicPrivateRoute,
} from './component/custom-route';

import {
  Signup,
  Signin,
  HomeLoggedIn,
  Home,
} from './page/';

import Cart from './page/cart/cart';

import Profile from './page/profile/profile';

import {
  Transaction
} from './page/transaction/index';

import {MerchantDetailPage} from './page/customer/index';


function NotFound({ location }) {
  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

class App extends Component {
  // If you want to test the logged in state, you can change the data below
  loggedIn = () => {
    this.setState({
      isLoggedIn: true,
    })
  }

  render() {
    return (
    <Router>
      <main>
        <NavBar isLoggedIn={this.props.isLoggedIn}/>
        <Switch>
          <PublicPrivateRoute path="/" exact publicRender={Home} privateRender={HomeLoggedIn} isAuthenticated={this.props.isLoggedIn} />
          <Route path="/signin" render={(props) => <Signin {...props} loggedIn={this.loggedIn}/> } />
          <Route path="/signup" render={(props) => <Signup {...props} loggedIn={this.loggedIn}/> } />
          <PrivateRoute path="/transaction" pathFail="/signin" isAuthenticated={this.props.isLoggedIn} render={Transaction} /> 
          <PrivateRoute path="/profile/:user_id" pathFail="/signin" isAuthenticated={this.props.isLoggedIn} render={Profile} />
          <PrivateRoute path="/cart" pathFail="/signin" isAuthenticated={this.props.isLoggedIn} render={()=> <Cart/>} />
          <PrivateRoute path="/merchant/:merchant_id" pathFail="/signin" isAuthenticated={this.props.isLoggedIn} render={() => <MerchantDetailPage/>} />
          <Route component={NotFound} />
        </Switch>
      </main>
    </Router>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.isLoggedIn
  }
}

export default connect(mapStateToProps)(App);