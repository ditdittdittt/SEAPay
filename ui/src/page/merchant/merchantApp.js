import React,{Component} from 'react';
import { Row, Card } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {AddProductCard,ProductListCard} from './index'
import {connect} from 'react-redux';

class MerchantApp extends Component{
    removeProduct = (id) => {
        const tempProductList = this.props.productList.filter(obj =>{
            return obj.product_id !== id;})
        this.props.updateProductList(tempProductList);
    }
    addProduct = (obj) => {
        const tempProductList = [...this.props.productList,obj];
        console.log(tempProductList)
        this.props.updateProductList(tempProductList);
    }
    render(){
        const cardUserDetailStyle = {
            "border" : "grey solid 1.2pt",
            "width":"24rem",
            margin:0};
        return(
            <Row id="merchant-app" className="mt-4 justify-content-center">
                <div className="m-2">
                    <Card style={cardUserDetailStyle} className="mx-auto">
                        <Card.Body className="px-3 pt-3 pb-1">
                            <Card.Title style={{"marginBottom" : 12,fontSize:"16pt"}}>{this.props.name}</Card.Title>
                            <Card.Subtitle>Merchant</Card.Subtitle>
                            <Card.Text className="mt-3" style={{fontSize:"14pt"}}>
                                SEA Pay : Rp. {this.props.moneyBalance}
                            </Card.Text>
                        </Card.Body>
                        <Card.Body style={{fontWeight:"450",fontSize:"14pt"}}>
                            <Card.Text className="text-center hover-grey float-right"><Link to="" className="hover-grey">History<br/>Transaction</Link></Card.Text>
                        </Card.Body>
                    </Card>
                    <AddProductCard addProduct={this.addProduct} />
                </div>
                <div className="m-2">
                    <h3 className="text-center">Product List</h3>
                    <ProductListCard productList={this.props.productList} removeFunc={this.removeProduct}/>
                </div>
            </Row>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProductList: (obj) => {dispatch({type:"UPDATE_PRODUCT_LIST",detail:obj})}
    }
}

const mapStateToProps = (state) => {
    return {
        productList:state.user_detail.product_list,
        name:state.user_detail.name,
        moneyBalance:state.user_detail.balance,
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(MerchantApp); 