import React, {Component} from 'react';
import { Container, Card, Col, Button, Row, Form, InputGroup } from 'react-bootstrap';

export default class Transaction extends Component{
    state={
        type:"TopUp",
        ammount:0,
        to:"",
        validated:false
    }
    handleSubmit = (e) => {
        const form = e.currentTarget;
        if (form.checkValidity() === false){
            e.preventDefault();
            e.stopPropagation();
        }else {
            e.preventDefault(); //stop refresh page
            e.stopPropagation();
        }
        this.setState({
            validated:true
        })
    }
    handleInput = (e) => {
        const isNumber = (text) => {return /^\d+$/.test(text);} //check number or not
        this.setState({
            [e.target.id] : (isNumber(e.target.value) ? parseInt(e.target.value) : e.target.value)
        })
    }
    render(){
        const cardStyle = {
            "border" : "grey solid 1.2pt",
            width:"25rem"
        }
        return(
            <Container>
                <Row className="justify-content-center">
                    <Col xs={5}>
                        <div className="mb-3 text-center">
                            <Button 
                                variant={this.state.type === "TopUp" ? "secondary" : "outline-secondary"}
                                className="mx-2"
                                onClick={() => {this.setState({type:"TopUp",validated:false})}}>
                                Top Up
                            </Button>
                            <Button 
                                variant={this.state.type === "Transfer" ? "secondary" : "outline-secondary"}
                                className="mx-2"
                                onClick={() => {this.setState({type:"Transfer",validated:false})}}>
                                Transfer
                            </Button>
                        </div>
                        <Card style={cardStyle} className="mx-auto">
                            <Card.Body>
                                <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}>
                                    <Form.Group as={Row} controlId="ammount">
                                        <Form.Label column sm="4" className="pr-0">Ammount :</Form.Label>
                                        <InputGroup as={Col} sm="8" className="pl-0">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text id="basic-addon1">Rp.</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control required type="number" min={15000} placeholder="Ammount" style={{borderTopRightRadius:"5px",borderBottomRightRadius:"5px"}} className="no-spin-button" onChange={this.handleInput} value={this.state.ammount} />
                                            <Form.Control.Feedback type="invalid">Minimal {this.state.type === "TopUp" ? "Top Up is 15000!" : "Transfer is 10000!"} </Form.Control.Feedback>
                                        </InputGroup>
                                    </Form.Group>
                                    {this.state.type === "Transfer" ? <Form.Group as={Row} controlId="to">
                                        <Form.Label column sm="2" className="pr-0">To :</Form.Label>
                                        <Col sm="10">
                                            <Form.Control required type="text" placeholder="Username" className="no-spin-button" onChange={this.handleInput} value={this.state.to} />
                                            <Form.Control.Feedback type="invalid">Input transfer destination!</Form.Control.Feedback>
                                        </Col>
                                    </Form.Group>  : null}
                                    <Button className="float-right" variant="primary" type="submit">
                                       {this.state.type === "TopUp" ? "Top Up" : "Transfer"}
                                    </Button>
                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        )
    }
}