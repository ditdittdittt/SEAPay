import React,{Component} from 'react';
import { Card, Container, Row, Col, Button, Alert } from 'react-bootstrap';
import {ProductCard,VoucherListModal} from './index';
import {connect} from 'react-redux';

class Cart extends Component {
    state={
        voucherType:null,
        voucherAmmount:0,
        voucher_id:null,
        voucherModalShow:false,
        showAlert:false
    }
    getVoucherDetail = (obj) =>{
        this.setState({
            voucherType:obj.type,
            voucherAmmount:obj.ammount,
            voucher_id:obj.id,
            voucherModalShow:false
        })
    }
    clearVoucher = ()=>{
        this.setState({
            voucherType:null,
            voucherAmmount:0,
            voucher_id:null,
        })
    }
    payFunc = () => {
        this.setState({
            showAlert:true
        })
    }
    totalPriceProduct = () => {
        let totalPrice = 0;
        this.props.productList.forEach(obj => {
            totalPrice += obj.totalPrice;
        })
        return this.applyVouchertoPrice(totalPrice);
    }
    applyVouchertoPrice = (price) => {
        let tempPrice = 0;
        switch (this.state.voucherType) {
            case "Discount":
                tempPrice = price*(100-this.state.voucherAmmount)/100;
                break;
            default:
                tempPrice = price;
                break;
        }
        return tempPrice;
    }
    closeVoucherModal = () => {
        this.setState({
            voucherModalShow:false
        })
    }
    openVoucherModal = () => {
        this.setState({
            voucherModalShow:true
        })
    }         
    render(){
        const cardStyle = {
            "border" : "grey solid 1.2pt",
            height:"auto",
            maxWidth:"100%"
        }
        const totalPrice = this.totalPriceProduct();
        return(
            <Container>
                <Row className="justify-content-center">
                    <Col xs={7}>
                        <h3 className="text-center mb-3">Cart</h3>
                        <Card style={cardStyle} className="mx-auto">
                            <Card.Body className="px-3 pt-3 pb-0">
                                <div className="scroll-hover" style={{height:"37rem"}}>
                                {this.props.productList.map(obj => {
                                    return(<ProductCard productDetail={obj} key={obj.product_id} />);
                                })}
                                </div>
                            </Card.Body>
                            <Container fluid style={{borderTop:"grey solid 1.2pt"}}>
                                <Row className="align-items-center justify-content-center pt-2">
                                    <Col xs={4}>
                                        <h5 className="text-left my-1">Total Price : {totalPrice}</h5>
                                    </Col>
                                    <Col xs={{span:2,offset:5}} className="text-center">
                                        <Button variant="secondary" style={{fontSize:"12pt"}} onClick={this.payFunc}>Pay</Button>
                                    </Col>
                                </Row>
                                <Row className="justify-content-center">
                                    <Col xs={4} className="p-0 text-center">
                                        <button className="no-style-button hover-grey my-2" onClick={this.openVoucherModal}><h6 className="mb-0">{this.state.voucherType !== null ? "Change" : "Add"}<br/>Voucher</h6></button>
                                        <button className="no-style-button hover-grey my-2" style={{display:(this.state.voucherType !== null ? "" : "none")}} onClick={this.clearVoucher}><h6 className="mb-0 text-center">Remove<br/>Voucher</h6></button>
                                    </Col>
                                    <Col xs={{span:5,offset:3}} className="text-center align-self-center">
                                        <h6 style={{display:(this.state.voucherType !== null ? "" : "none")}} className="my-2 float-left">Used Voucher : {this.state.voucherType+" "+this.state.voucherAmmount+"%"}</h6>
                                    </Col>
                                </Row>
                            </Container>
                            <Alert show={this.state.showAlert} variant="danger"  className="mx-auto mt-2" style={{width:"85%"}}>Balance not Enough!</Alert>
                        </Card>
                    </Col>
                </Row>
                <VoucherListModal show={this.state.voucherModalShow} onHide={this.closeVoucherModal} getVoucherDetailFunc={this.getVoucherDetail}/>
            </Container>
        );
    }
    
}

const mapStateToProps = (state) => {
    return {
        productList : state.cart
    }
}

export default connect(mapStateToProps)(Cart);