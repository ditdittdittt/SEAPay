import React from 'react';
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

const VoucherCard = ({voucherDetail,getVoucherDetailFunc}) => {
    const styleVoucher={
        "border" : "grey solid 1.2pt",
        borderRadius : "5px",
        "width":"21rem",
        height:"5.5rem"};
    return(
        <Card style={styleVoucher} className="mb-2 mx-auto hoverable" onClick={getVoucherDetailFunc}>
            <Card.Body  className="p-3">
                <Card.Title><Link to="" className="hover-grey">{voucherDetail.name}</Link></Card.Title>
                <Card.Subtitle className="text-right"  >{voucherDetail.type+" : "+voucherDetail.ammount+"%"}</Card.Subtitle>
            </Card.Body>
        </Card>
    );
}

export default VoucherCard;