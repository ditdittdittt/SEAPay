import React,{Component} from 'react';
import { Container, Row, Col, Card, Form, Button, Alert } from 'react-bootstrap';
import {connect} from 'react-redux';

class Profile extends Component {
    state={
        user_id:null,
        email:"",
        name:"",
        oldPassword:"",
        password:"",
        description:"",
        type:"Customer",
        confirmPassword:null,
        validated:false
    }
    componentDidMount(){
        this.setState({
            user_id:this.props.computedMatch.params.user_id,
            email:this.props.email,
            name:this.props.name,
            description:this.props.description,
        })
    }
    handleSubmit = (e) => {
        if(this.state.confirmPassword){
            const form = e.currentTarget;
            if (form.checkValidity() === false){
                e.preventDefault();
                e.stopPropagation();
            }else {
                e.preventDefault(); //stop refresh page
                e.stopPropagation();
            }
            this.setState({
                validated:true
            })
        }else{
            e.preventDefault();
            e.stopPropagation();
        }
    }
    checkPassword = (e) => {
        this.setState({
            confirmPassword: e.target.value === this.state.password,
        })
    }
    handleInputChange = (e) =>{
        this.setState({
            [e.target.id]:e.target.value
        })
    }
    render(){
        const cardStyle = {
            "border" : "grey solid 1.2pt",
            width:"27rem",
        }
        return(
            <Container>
                <Row className="justify-content-center">
                    <Col xs={6} >
                        <h3 className="text-center mb-3">Profile,UserId: {this.state.user_id}</h3>
                        <Card style={cardStyle} className="mx-auto">
                            <Card.Body className="p-3">
                                <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}>
                                    <Form.Group controlId="email">
                                        <Form.Label>Email Address</Form.Label>
                                        <Form.Control value={this.state.email} required type="email" placeholder="Enter email" onChange={this.handleInputChange}/>
                                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">Email required!</Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group controlId="name">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control value={this.state.name} required type="text" placeholder="Enter your name" onChange={this.handleInputChange}/>
                                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">Name required!</Form.Control.Feedback>
                                    </Form.Group>
                                    {this.props.type === "Merchant" ?                                     
                                    <Form.Group controlId="description">
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control required as="textarea" placeholder="Description" value={this.state.description} onChange={this.handleInputChange}/>
                                        <Form.Control.Feedback type="invalid">Description required!</Form.Control.Feedback>
                                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                    </Form.Group>: null}
                                    <Form.Group controlId="oldPassword">
                                        <Form.Label>Old Password</Form.Label>
                                        <Form.Control value={this.state.oldPassword} required type="password" placeholder="Password" onChange={this.handleInputChange}/>
                                        <Form.Control.Feedback type="invalid">Password required!</Form.Control.Feedback>
                                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group controlId="password">
                                        <Form.Label>New Password</Form.Label>
                                        <Form.Control value={this.state.password} required type="password" placeholder="Password" onChange={this.handleInputChange}/>
                                        <Form.Control.Feedback type="invalid">Password required!</Form.Control.Feedback>
                                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group controlId="ConfirmPassword">
                                        <Form.Label>Confirm Password</Form.Label>
                                        <Form.Control required type="password" placeholder="Retype Password" onChange={this.checkPassword}/>
                                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">Retype the password!</Form.Control.Feedback>
                                            <Alert className="mt-2" variant={this.state.confirmPassword ? "success" : "danger" } style={{"transition":"0.5s"}} show={this.state.confirmPassword !== null}>
                                                Password is {this.state.confirmPassword ? "same." : "different!" }
                                            </Alert>
                                    </Form.Group>
                                    <Button variant="primary" type="submit" className="float-right">
                                        Submit
                                    </Button>
                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        name:state.user_detail.name,
        email:state.user_detail.email,
        type:state.type,
        description:state.user_detail.description
    }
}

export default connect(mapStateToProps)(Profile);