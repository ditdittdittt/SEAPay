import React,{useState} from 'react';
import { Card } from "react-bootstrap";
import VoucherDetailModal from './userVoucherDetailModal';


const VoucherCard = ({voucherDetail}) => {
    const [ShowModal, setShowModal] = useState(false);
    const styleVoucher={
        "border" : "grey solid 1.2pt",
        borderRadius : "5px",
        "width":"21rem",
        height:"5.5rem"};
    return(
        <div>
            <Card style={styleVoucher} className="mt-2 mx-auto">
                <Card.Body  className="p-3">
                    <Card.Title><button onClick={() => {setShowModal(true)}} className="hover-grey no-style-button p-0" style={{fontWeight:"500"}}>{voucherDetail.title}</button></Card.Title>
                    <Card.Subtitle className="text-right"  >{voucherDetail.type+" : "+voucherDetail.ammount+"%"}</Card.Subtitle>
                </Card.Body>
            </Card>
            <VoucherDetailModal show={ShowModal} onHide={() => {setShowModal(false)}} voucherDetail={voucherDetail} />
        </div>
    );
}

export default VoucherCard;