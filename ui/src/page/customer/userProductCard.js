import React,{useState} from 'react';
import { Card, Row,Col } from "react-bootstrap";
import ProductDetailModal from './userProductDetailModal';

const ProductCard = ({productDetail,addToCart}) => {
    const [ShowModal, setShowModal] = useState(false);
    const productDescription = (productDetail.description.length > 80 ? productDetail.description.slice(0,80) + "...." : productDetail.description );
    const styleProduct={
        "border" : "grey solid 1.2pt",
        "width":"20rem",
        height:"13rem",
    display:"inline-block",
        margin:"8px",
        cursor:"pointer"};
    const wrappedAddToCart = (obj,id) => {
        setShowModal(false);
        addToCart(obj,id);
    }
    return(
        <div>
            <Card style={styleProduct} onClick={() => {setShowModal(true)}}>
                <Card.Body>
                    <Row className="mb-2">
                        <Col md={5} className="align-self-center">
                            <Card.Title className="mb-0"><button className="p-0 hover-grey no-style-button" style={{fontWeight:"bold"}}>{productDetail.name}</button></Card.Title>
                        </Col>
                    </Row>
                    <Card.Text style={{height:"4.5rem"}}>{productDescription}</Card.Text>
                </Card.Body>
                <Card.Body>
                    <Row className="justify-content-center">
                        <Col xs={5} className="align-self-center p-0 text-center">
                            <Card.Subtitle style={{fontSize:"14pt"}}>{"Rp. "+productDetail.price}</Card.Subtitle>
                        </Col>
                        <Col xs={{offset:1,span:6}} className="align-self-center p-0 text-center">
                            <h6 className="mb-0">Stock : {productDetail.stock} Units</h6>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            {ShowModal ? <ProductDetailModal addToCart={wrappedAddToCart} productDetail={productDetail} show={ShowModal} onHide={() => {setShowModal(false)}} /> : null}
        </div>
    )
}

export default ProductCard;