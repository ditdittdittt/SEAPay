import React,{Component} from 'react';
import { Modal,Row,Col,Image,Container, Button} from 'react-bootstrap';
import Plus from '../asset/baseline-add-24px.svg';
import Remove from '../asset/baseline-remove-24px.svg';
import {connect} from 'react-redux';

class ProductDetailModal extends Component {
    state ={
        ammount:0,
        totalPrice:0,
        stock:0
    }
    componentDidMount(){
        this.setState(
            {
                ammount:(this.props.ammount === null ? 0 : this.props.ammount),
                totalPrice:(this.props.totalPrice === null ? 0 : this.props.totalPrice),
                stock:this.props.productDetail.stock
        }
        )
    }
    addFunction = () => {
        if(this.state.stock !== 0){
            this.setState({
                ammount:this.state.ammount + 1,
                totalPrice:(this.state.ammount+1)*this.props.productDetail.price,
                stock:this.state.stock -1 
            })
        }
    }
    reduceFunction = () => {
        if(this.state.ammount > 0){
            this.setState({
                ammount:this.state.ammount - 1,
                totalPrice:(this.state.ammount-1)*this.props.productDetail.price,
                stock:this.state.stock +1 
            })
        }
    }
    addItemToCart = () => {
        this.props.addToCart({
            name:this.props.productDetail.name,
            price:this.props.productDetail.price,
            ammount:this.state.ammount,
            totalPrice:this.state.totalPrice,
            stock:this.state.stock,
            product_id:this.props.productDetail.product_id,
            description:this.props.productDetail.description
        },this.props.productDetail.product_id);
        this.props.updateProduct({
            ...this.props.productDetail,
            stock:this.state.stock
        })
    }
    render(){
        const styleText ={
            fontWeight:"normal",
            overflowWrap:"break-word"
        }
        const {name,price,stock,description} = this.props.productDetail;
        const totalPrice=this.state.totalPrice;
        return(
            <Modal show={this.props.show} onHide={this.props.onHide} aria-labelledby="contained-modal-title-vcenter"
          centered scrollable>
                <Modal.Header closeButton>
                    <Modal.Title>{name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        <h4 style={styleText}>Stock: {stock} Units</h4>
                        <h4 style={styleText}>Price: Rp. {price}</h4>
                        <h4 style={styleText}>Description: {description}</h4>
                </Modal.Body>
                <Modal.Footer>
                    <Container>
                        <Row className="justify-content-center" style={{marginBottom:"20px"}}>
                            <Col xs={5} className="align-self-center">
                                <Row>
                                    <Col xs={3} className="p-0 text-center">
                                        <button className="no-style-button p-0 hoverable" onClick={this.reduceFunction}><Image fluid src={Remove}/></button>
                                    </Col>
                                    <Col xs={3} className="p-0 align-self-center text-center">
                                        <h5 className="text-center mb-0">{this.state.ammount}</h5>
                                    </Col>
                                    <Col xs={3} className="p-0 text-center">
                                        <button className="no-style-button p-0 hoverable" onClick={this.addFunction}><Image fluid src={Plus}/></button>
                                    </Col>  
                                </Row>
                            </Col>
                            <Col xs={{span:4,offset:3}} className="align-self-center">
                                <h5 className="text-center m-0">Total Price : Rp. {totalPrice}</h5>
                            </Col>
                        </Row>
                        <Button variant="secondary" onClick={this.addItemToCart} className="float-right">{this.props.inTheCart ? "Update Cart" : "Add To Cart"} </Button>
                    </Container>
                </Modal.Footer>
            </Modal>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProduct : (obj) => {dispatch({type:"UPDATE_PRODUCT_STOCK",detail:obj})}
    }
}
const mapStateToProps = (state,ownProps) => {
    let cartObj = (state.cart.find(obj => obj.product_id === ownProps.productDetail.product_id) === undefined ? null : state.cart.find(obj => obj.product_id === ownProps.productDetail.product_id));
    return {
        inTheCart : (cartObj === null ? false : true),
        ammount:(cartObj === null ? null : cartObj.ammount),
        totalPrice:(cartObj === null ? null : cartObj.totalPrice),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ProductDetailModal);