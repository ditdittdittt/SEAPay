import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import {Link} from 'react-router-dom';

const MerchantCard = ({merchantDetail}) => {
    const styleCard={
        "border" : "grey solid 1.2pt",
        "width":"21rem",
        height:"auto"};
    return(
        <div>
            <Card className="mt-2 mx-auto" style={styleCard} >
                <Card.Body>
                    <Row>
                        <Col sm={7} className="align-self-center">
                            <Card.Title className="mb-0"><Link to={"/merchant/"+merchantDetail.merchant_id} className="hover-grey no-style-button" style={{fontWeight:500}}>{merchantDetail.name}</Link></Card.Title>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        </div>
    )
}

export default MerchantCard;