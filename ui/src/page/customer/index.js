import Setting from './setting';
import CustomerApp from './customerApp';
import MerchantDetailPage from './merchantDetailPage';

export {
    Setting,
    CustomerApp,
    MerchantDetailPage
}