import React,{Component} from 'react';
import {Row, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import VoucherList from './voucherList';
import {connect} from 'react-redux';
import MerchantListCard from './merchantListCard';

class CustomerApp extends Component{
    render(){
        const cardUserDetailStyle = {
            "border" : "grey solid 1.2pt",
            "width":"25rem",
            margin:0};
        const cardTransactionMenuStyle = {
            display:"inline-flex",
            flexWrap:"nowrap",
            flexDirection:"row",
            fontSize:"14pt",
            fontWeight:"450"
        }
        let levelColour = null;
        switch (this.props.level) {
            case "Gold":
                levelColour = {color:"#F1C708"};
                break;
            case "Silver":
                levelColour = {color:"#676663"};
                break;
            case "Platinum":
                levelColour = {color:"#262152"};
                break;
            default:
                break;
        }
        return(
            <Row id="customer-app" className={"mt-3 justify-content-center "+this.props.className} style={this.props.style} >
                <div className="mx-2">
                    <Card style={cardUserDetailStyle} className="mb-2 mx-auto" >
                        <Card.Body className="px-3 pt-3 pb-1">
                            <Card.Title style={{"marginBottom" : 12,fontSize:"16pt"}}>{this.props.name}</Card.Title>
                            <Card.Subtitle>User <span style={levelColour} className="mx-1">{this.props.level}</span></Card.Subtitle>
                            <Card.Text className="mt-3" style={{fontSize:"14pt"}}>
                                SEA Pay : Rp. {this.props.moneyBalance}<br/>
                                SEA Point : {this.props.seaPoint} Point<br/>
                                Loyality : {this.props.loyalityPoint}
                            </Card.Text>
                        </Card.Body>
                        <Card.Body style={cardTransactionMenuStyle} className="justify-content-center">
                                <Card.Text className="align-self-center px-3"><Link to="" className="hover-grey">TopUp</Link></Card.Text>
                                <Card.Text className="align-self-center px-3 py-2" style={{borderLeft:"grey solid 1.5pt",borderRight:"grey solid 1.5pt"}}><Link to="" className="hover-grey">Transfer</Link></Card.Text>
                                <Card.Text className="text-center px-3 hover-grey"><Link to="" className="hover-grey">History Transaction</Link></Card.Text>
                        </Card.Body>
                    </Card>
                    <VoucherList voucherList={this.props.voucherList}/>
                </div>
                <div className="mx-2">
                    <h3 className="text-center">Merchant List</h3>
                    <MerchantListCard />
                </div>
            </Row>
        );
    }
}

const mapStateToProps =(state) => {
    return {
        name:state.user_detail.name,
        seaPoint:state.user_detail.sea_point,
        loyalityPoint:state.user_detail.loyality_point,
        level:state.user_detail.level,
        voucherList:state.user_detail.voucher_list,
        moneyBalance:state.user_detail.balance,
    }
}

export default connect(mapStateToProps)(CustomerApp);