import React,{Component} from 'react';
import { Container, Row, Card } from 'react-bootstrap';
import ProductCard from './userProductCard';
import {connect} from 'react-redux';

class MerchantDetailPage extends Component {
    state={
        name:"Anugerah 2.0",
        description:"asdasdasfsafsafsafasdsahglkadshglkahdslgkh",
    /*
     cart obj
     {
        name:
        price:
        ammount:
        totalPrice:
        stock:
        product_id:
     }
    */
    }
    addItemToCart = (obj,id) => {
        const checkId = (id) => {
            const checker = this.props.cart.filter(object => {
                return id === object.product_id;
            })
            return checker.length > 0;
        }
        if(checkId(id)){
            this.props.updateToCart(obj)
        }else{
            this.props.addToCart(obj);
        }
    }
    componentDidMount(){
        //GET API
    }
    render(){
        const productListStyle = {
            "border" : "grey solid 1.2pt",
            "maxWidth":"65.7rem",
            height:"40rem"
        };
        const merchantDetailStyle ={
            "border" : "grey solid 1.2pt",
        }
        return(
            <Container fluid>
                <Row className="justify-content-center">
                    <div className="align-self-start m-2">
                        <Card style={merchantDetailStyle} className="mx-auto">
                            <Card.Body>
                            <h5 className="text-center mb-3">Merchant Detail</h5>
                                <Card.Title>{this.state.name}</Card.Title>
                                <Card.Subtitle>{this.state.description}</Card.Subtitle>
                            </Card.Body>
                        </Card>
                    </div>
                    <div className="align-self-center m-2">
                        <h3 className="text-center">Product List</h3>
                        <Card style={productListStyle} className="mx-auto">
                            <Card.Body>
                                <div className="scroll-hover" style={{width:"auto",height:"37rem",display:"flex",flexWrap:"wrap"}}>
                                    {this.props.productList.map(obj => {
                                        return(
                                            <ProductCard key={obj.product_id} productDetail={obj} addToCart={this.addItemToCart}/>
                                        )
                                    })}
                                </div>
                            </Card.Body>
                        </Card>
                    </div>
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cart:state.cart,
        productList:state.merchant_detail.product_list
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (obj) => { dispatch({type:"ADD_TO_CART",detail:obj})},
        updateToCart : (obj) => { dispatch({type:"UPDATE_TO_CART",detail:obj})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(MerchantDetailPage);