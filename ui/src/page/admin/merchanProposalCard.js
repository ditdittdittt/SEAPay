import React,{useState} from 'react';
import { Card, Row, Col, Image } from 'react-bootstrap';
import Check from '../asset/baseline-check-24px.svg';
import Close from '../asset/baseline-close-24px.svg';
import MerchantDetailModal from './merchantDetailModal';

const MerchantProposalCard = ({merchantDetail}) => {
    const [showModal, setShowModal] = useState(false);
    const styleCard={
        "border" : "grey solid 1.2pt",
        "width":"21rem",
        height:"auto"};
    return(
        <div>
            <Card className="mt-2 mx-auto" style={styleCard} >
                <Card.Body>
                    <Row>
                        <Col sm={7} className="align-self-center">
                            <Card.Title className="mb-0"><button onClick={() => {setShowModal(true)}} className="hover-grey no-style-button" style={{fontWeight:500}}>{merchantDetail.name}</button></Card.Title>
                        </Col>
                        <Col sm={{offset:1,span:4}} >
                            <Row className="align-self-center">
                                <Image fluid  width={35} height={35} className="mx-2" style={{cursor:"pointer"}} src={Check}/>
                                <Image fluid width={35} height={35} className="mx-1" style={{cursor:"pointer"}} src={Close}/>
                            </Row>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            <MerchantDetailModal show={showModal} merchantDetail={merchantDetail} onHide={() => {setShowModal(false)}}/>
        </div>
    )
}

export default MerchantProposalCard;